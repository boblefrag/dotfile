;;; prog-config --- Main programing utilities


;; smart Buffer mode
(require 'ido)
(ido-mode t)
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(setq-default indent-tabs-mode nil)
(global-whitespace-mode t)
(require 'indent-guide)
(indent-guide-global-mode)
(setq json-reformat:indent-width 2)

;; Linter and beautifiers
(require 'flymake-python-pyflakes)
(require 'py-yapf)
;; I don't want to get outdated linters and beaufiers.
;; But I don't want to pip install system wide
;; so I create a virtualenv for the linting
;; pip install flake8 yapf
;; yapf syle definition is copied form showwebgl to /home/yohann/.config/yapf/style
(add-to-list 'load-path "~/.emacs/venv/bin/")
(setq exec-path (append exec-path '("~/.emacs/venv/bin/")))
(setq flymake-python-pyflakes-executable "~/.emacs/venv/bin/flake8")
(global-flycheck-mode)

(add-hook 'python-mode-hook 'flymake-python-pyflakes-load)
(add-hook 'before-save-hook 'whitespace-cleanup)

(defun beautify-python ()
  (py-isort-buffer)
  (message "applied isort")
  (py-yapf-buffer)
  (message "applied yapf")
  )

(defun py-beautify-before-save ()
  (interactive)
  (when (eq major-mode 'python-mode)
    (condition-case err (beautify-python)
      (error (message "%s" (error-message-string err))))))

(add-hook 'before-save-hook 'py-beautify-before-save)


;;Autocomplete, snippets, syntaxe hightlight

(require 'pyvenv)
;; Custom scripts

;; beautify json

(defun beautify-json ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -mjson.tool" (current-buffer) t)))


(global-set-key (kbd "M-#") 'comment-region)
(global-set-key (kbd "C-#") 'uncomment-region)

(global-git-gutter-mode)

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "iceweasel")

(defun yaml2json ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)'"(current-buffer) t)))

(defun json2yaml ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -c 'import sys, yaml, json; yaml.safe_dump(json.load(sys.stdin), sys.stdout, indent=2)'"(current-buffer) t)))

(eval-after-load "sql"
  (load-library "sql-indent"))


(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:setup-keys t)
(setq jedi:complete-on-dot t)

(provide 'prog-config)
