install:
	cp .emacs ~
	cp -R .emacs.d ~
	virtualenv ~/.emacs.d/venv
	~/.emacs.d/venv/bin/pip install -r ~/.emacs.d/requirements.txt
