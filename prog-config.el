 ;;smart Buffer mode
(require 'ido)
(ido-mode t)
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(setq-default indent-tabs-mode nil)
(global-whitespace-mode t)

;;Autocomplete, snippets, syntaxe hightlight

(require 'yasnippet)
(yas-global-mode 1)
(require 'auto-complete)
(require 'auto-complete+)
(add-to-list 'ac-dictionary-directories "~/emacs.d/el-get/auto-complete/dict")
(setq-default ac-sources (add-to-list 'ac-sources 'ac-source-dictionary))
(setq ac-auto-start 2)
(setq ac-ignore-case nil)
(global-auto-complete-mode t)

(require 'flymake-python-pyflakes)
(add-hook 'python-mode-hook 'flymake-python-pyflakes-load)

;; Auto Syntax Error Hightlight

 (defun show-fly-err-at-point ()
   "If the cursor is sitting on a flymake error, display the
 message in the minibuffer"
   (interactive)
   (let ((line-no (line-number-at-pos)))
     (dolist (elem flymake-err-info)
       (if (eq (car elem) line-no)
           (let ((err (car (second elem))))
             (message "%s" (fly-pyflake-determine-message err)))))))

 (defun fly-pyflake-determine-message (err)
   "pyflake is flakey if it has compile problems, this adjusts the
 message to display, so there is one ;)"
   (cond ((not (or (eq major-mode 'Python) (eq major-mode 'python-mode) t)))
         ((null (flymake-ler-file err))
          ;; normal message do your thing
          (flymake-ler-text err))
         (t ;; could not compile err
          (format "compile error, problem on line %s" (flymake-ler-line err)))))

 (defadvice flymake-goto-next-error (after display-message activate compile)
   "Display the error in the mini-buffer rather than having to mouse over it"
   (show-fly-err-at-point))

 (defadvice flymake-goto-prev-error (after display-message activate compile)
   "Display the error in the mini-buffer rather than having to mouse over it"
   (show-fly-err-at-point))

 (defadvice flymake-mode (before post-command-stuff activate compile)
   "Add functionality to the post command hook so that if the
 cursor is sitting on a flymake error the error information is
 displayed in the minibuffer (rather than having to mouse over
 it)"
   (set (make-local-variable 'post-command-hook)
        (cons 'show-fly-err-at-point post-command-hook)))

(when (load "flymake" t)
  (defun flymake-jslint-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
		       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "jslint" (list "--terse" local-file))))

  (setq flymake-err-line-patterns
	(cons '("^\\(.*\\)(\\([[:digit:]]+\\)):\\(.*\\)$"
		1 2 nil 3)
	      flymake-err-line-patterns))

  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.js\\'" flymake-jslint-init))

)


(defun flymake-html-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
       'flymake-create-temp-inplace))
       (local-file (file-relative-name
       temp-file
       (file-name-directory buffer-file-name))))
    (list "tidy" (list local-file))))
(add-to-list 'flymake-allowed-file-name-masks
       '("\\.html$\\|\\.ctp" flymake-html-init))
       (add-to-list 'flymake-err-line-patterns
       '("line \\([0-9]+\\) column \\([0-9]+\\) - \\(Warning\\|Error\\): \\(.*\\)"
       nil 1 2 4))

;; pony-mode
(add-to-list 'load-path "~/.emacs.d/el-get/pony-mode/src")
(require 'pony-mode)
(require 'pyvenv)
;; Custom scripts

;; beautify json

(defun beautify-json ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -mjson.tool" (current-buffer) t)))


(global-set-key (kbd "M-#") 'comment-region)
(global-set-key (kbd "C-#") 'uncomment-region)

(global-git-gutter+-mode)

(provide 'prog-config)

