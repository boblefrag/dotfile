;;; emacs --- Summary
;;; Commentary:
; Base Emacs package
;;(load-file "~/.emacs.d/el-get/cedet-1.0pre6/common/cedet.el")
;;mac osX fixes
;;(set-frame-parameter (selected-frame) 'alpha '(85 0))

;; (setq mac-option-modifier nil
;;       mac-command-modifier 'meta
;;       x-select-enable-clipboard t)
;;; Code:
(set-face-attribute 'default nil :height 110)
(setq-default fill-column 100)
(setq whitespace-line-column 100)
(add-to-list 'exec-path "/usr/local/bin")


(let ((default-directory "~/.emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))


(setq package-archives '(("elpa" . "http://tromey.com/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ))


; activate all the packages (in particular autoloads)
(package-initialize)

(setq required-pkgs '(markdown-mode django-snippets yasnippet
gnuplot gnuplot-mode go-mode helm-pydoc pyvenv flycheck
git-gutter auto-complete org org-plus-contrib yasnippet magit
ox-reveal ox-rst yaml-mode indent-guide flymake-python-pyflakes py-yapf sql-indent jedi py-isort))

(require 'cl)

(setq pkgs-to-install
      (let ((uninstalled-pkgs (remove-if 'package-installed-p required-pkgs)))
        (remove-if-not '(lambda (pkg) (y-or-n-p (format "Package %s is missing. Install it? " pkg))) uninstalled-pkgs)))

(when (> (length pkgs-to-install) 0)
  (package-refresh-contents)
  (dolist (pkg pkgs-to-install)
    (package-install pkg)))


;; ;;Autocomplete, snippets, syntaxe hightlight, custom keybinding

(require 'prog-config)

;; ;; plus de couleurs

(global-font-lock-mode t)

;; ;; numéro de colonne et de ligne

(column-number-mode 1)
(line-number-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark)))
 '(inferior-lisp-program "clisp")
 '(org-agenda-files
   (quote
    ("~/.emacs.d/org-list/work.org" "~/.emacs.d/org-list/perso.org" "~/.emacs.d/org-list/manual.org" "~/.emacs.d/org-list/index.org")))
 '(py-yapf-options nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Latex part
;; The following lines are always needed.  Choose your own keys.

(require 'tramp)
(require 'org-config)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(set-background-color "black")
(require 'ido)
(require 'auto-complete-config)
(require 'yasnippet)

(setq yas-snippet-dirs
      '("~/.emacs.d/snippets")
      )
(yas-global-mode 1)

(ac-config-default)
(ido-mode t)


(setq initial-scratch-message "")
(setq inhibit-startup-message t)
(setq visible-bell t)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
