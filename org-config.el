(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(setq org-timer-default-timer 25)


;; ;; (require 'org-s5)
;; (add-to-list 'load-path "~/.emacs.d/el-get/org-mode/lisp/")
;; (add-to-list 'load-path "~/.emacs.d/el-get/org-mode/contrib/lisp" t)
;; ;;(require 'org-special-blocks)
(setq org-export-with-sub-superscripts nil)
(unless (boundp 'org-export-latex-classes)
  (setq org-export-latex-classes nil))
(add-to-list 'org-export-latex-classes
             '("book"
	       "\\documentclass{book}
               \\usepackage[utf8x]{inputenc}
               \\usepackage[T1]{fontenc}
               \\usepackage[french]{babel}
               \\usepackage[colorlinks=true,urlcolor=SteelBlue4,linkcolor=Firebrick4]{hyperref}
               \\newsavebox{\\mybox}
               \\newlength{\\mydepth}
               \\newlength{\\myheight}


               \\newenvironment{REMARQUE}
                {\\begin{lrbox}{\\mybox}\\begin{minipage}{0.7\\textwidth}}%
                {\\end{minipage}\\end{lrbox}
                \\settodepth{\\mydepth}{\\usebox{\\mybox}}%
                \\settoheight{\\myheight}{\\usebox{\\mybox}}%
                \\addtolength{\\myheight}{\\mydepth}%
                \\noindent\\makebox[90pt]{\\hspace{-0pt}\\rule[-\\mydepth]{5pt}{\\myheight}}%
                \\usebox{\\mybox}}
                "
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ))

(add-to-list 'org-export-latex-classes
  ;; beamer class, for presentations
  '("beamercustom"
     "\\documentclass[11pt]{beamer}\n
      \\usetheme{{{{beamertheme}}}}\n
      \\setbeameroption{show notes}
      \\usepackage[utf8]{inputenc}\n
      \\usepackage[T1]{fontenc}\n
      \\usepackage{hyperref}\n
      \\usepackage{color}
      \\usepackage[colorlinks=true,urlcolor=SteelBlue4,linkcolor=Firebrick4]{hyperref}
      \\usepackage{listings}
      \\usepackage{verbatim}\n
       \\subject{{{{beamersubject}}}}\n"

    ("\\section{%s}" . "\\section*{%s}")
     
     ("\\begin{frame}[fragile]\\frametitle{%s}"
       "\\end{frame}"
       "\\begin{frame}[fragile]\\frametitle{%s}"
       "\\end{frame}")))

  ;; letter class, for formal letters

  (add-to-list 'org-export-latex-classes

  '("letter"
     "\\documentclass[11pt]{letter}\n
      \\usepackage[utf8]{inputenc}\n
      \\usepackage[T1]{fontenc}\n
      \\usepackage{color}
      \\usepackage[colorlinks=true,urlcolor=SteelBlue4,linkcolor=Firebrick4]{hyperref}"

     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-export-latex-classes
 ;; cv class for my own curriculum vitae
 '("cv"
   "\\documentclass[11pt]{article}
    \\usepackage[utf8]{inputenc}
    \\usepackage[T1]{fontenc}
    \\usepackage[french]{babel}
    \\usepackage{titlesec}
    \\usepackage{color}
    \\usepackage[utf8]{inputenc}
    \\usepackage[colorlinks=true,urlcolor=SteelBlue4,linkcolor=Firebrick4]{hyperref}
\\titleformat{\\section}[block]
  {\\normalfont\\bfseries\\filcenter}{\\colorbox{SteelBlue4}{\\itshape\\thesection}}{4em}{} 
"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)


(setq org-agenda-files (quote ("~/.emacs.d/org-list/work.org"
                               "~/.emacs.d/org-list/perso.org"
			       "~/.emacs.d/org-list/manuel_perso_org-mode.org"
                               "~/.emacs.d/org-list/manual.org"
                               "~/.emacs.d/org-list/index.org"

			       )))

(setq org-directory "/home/yohann/.emacs.d/org-list/")




(setq org-publish-project-alist
      '(

       ;; ... add all the components here (see below)...
        ("presentations"
         :base-directory "~/.emacs.d/presentations/"
         :base-extension "org"
         :publishing-directory "~/public_html/"
         :recursive t
	 :publishing-function org-publish-org-to-html
         )
	("org-notes"
	 :base-directory "~/org/"
	 :base-extension "org"
	 :publishing-directory "~/public_html/"
	 :recursive t
	 :publishing-function org-publish-org-to-html
	 :headline-levels 4             ; Just the default for this project.
	 :auto-preamble t
	 )

        ("prez-static"
	 :base-directory "~/.emacs.d/presentations/"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "~/public_html/"
	 :recursive t
	 :publishing-function org-publish-attachment
	 )

	("org-static"
	 :base-directory "~/org/"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "~/public_html/"
	 :recursive t
	 :publishing-function org-publish-attachment
	 )
        ("org-personal"
         :base-directory "~/.emacs.d/org-list/"
         :base-extension "org"
         :publishing-directory "/ssh:yohann@dev.gabory.fr:/home/yohann/org-files/"
         :recursive t
         :publishing-function org-publish-org-to-html
         )
	("org"
	 :components ("org-notes" "org-static")
	 )
        ("prez"
         :components ("presentations" "prez-static")
         )

)
      )


(setq org-todo-keywords
  '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

(setq org-tag-alist '(
                      ("@work" . ?w) ("@home" . ?h) ("@perso" . ?p) ("@manual" . ?m)
                      ))

(setq org-directory "~/.emacs.d/org-list/")
(setq org-mobile-directory "~/Dropbox/MobileOrg")
(setq org-mobile-inbox-for-pull "~/.emacs.d/org-list/flagged.org")
(setq org-combined-agenda-icalendar-file "~/.emacs.d/org-list/agenda.ics")

(setq org-export-latex-emphasis-alist
      '(("*" "\\textbf{%s}" nil)
        ("/" "\\emph{%s}" nil)
        ("_" "\\underline{%s}" nil)
        ("+" "\\st{%s}" nil)
        ("=" "\\url{%s}" nil)
        ("~" "\\verb" t)))


(provide 'org-config)
